import { BrowserModule } from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthViewComponent } from './views/auth-view/auth-view.component';
import { ErrorViewComponent } from './views/error-view/error-view.component';
import { HeaderComponent } from './componants/header/header.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthService} from './services/auth/auth.service';
import { SeriesViewComponent } from './views/series-view/series-view.component';
import { TableComponent } from './componants/table/table.component';
import { TableLineComponent } from './componants/table-line/table-line.component';
import {HttpClientModule} from '@angular/common/http';
import {SerieService} from './services/serie/serie.service';
import { NewSerieViewComponent } from './views/new-serie-view/new-serie-view.component';
import { SignUpViewComponent } from './views/sign-up-view/sign-up-view.component';
import { SingleSerieViewComponent } from './views/single-serie-view/single-serie-view.component';
import { EditSerieViewComponent } from './views/edit-serie-view/edit-serie-view.component';
import {CommentComponent} from './componants/comment/comment.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthViewComponent,
    ErrorViewComponent,
    HeaderComponent,
    SeriesViewComponent,
    TableComponent,
    TableLineComponent,
    NewSerieViewComponent,
    SignUpViewComponent,
    SingleSerieViewComponent,
    EditSerieViewComponent,
    CommentComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [
    AuthService,
    SerieService,
],
  bootstrap: [AppComponent]
})
export class AppModule { }
