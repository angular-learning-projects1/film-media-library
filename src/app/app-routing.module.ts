import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthViewComponent} from './views/auth-view/auth-view.component';
import {ErrorViewComponent} from './views/error-view/error-view.component';
import {SeriesViewComponent} from './views/series-view/series-view.component';
import {AuthGuardService} from './services/guards/auth-guard.service';
import {NewSerieViewComponent} from './views/new-serie-view/new-serie-view.component';
import {SignUpViewComponent} from './views/sign-up-view/sign-up-view.component';
import {SingleSerieViewComponent} from './views/single-serie-view/single-serie-view.component';
import {EditSerieViewComponent} from './views/edit-serie-view/edit-serie-view.component';

const routes: Routes = [
  {path: 'auth', component: AuthViewComponent},
  {path: 'signup', component: SignUpViewComponent},
  {path: 'series', canActivate: [AuthGuardService], component: SeriesViewComponent},
  {path: 'serie/new', canActivate: [AuthGuardService], component: NewSerieViewComponent},
  {path: 'serie/edit/:id', canActivate: [AuthGuardService], component: EditSerieViewComponent},
  {path: 'serie/:id', canActivate: [AuthGuardService], component: SingleSerieViewComponent},
  {path: 'not-found', component: ErrorViewComponent},

  {path: '', redirectTo: 'auth', pathMatch: 'full'},
  {path: '**', redirectTo: 'not-found'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
