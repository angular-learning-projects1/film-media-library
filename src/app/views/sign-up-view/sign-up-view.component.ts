import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth/auth.service';
import {Router} from '@angular/router';
import * as firebase from 'firebase';

@Component({
  selector: 'app-sign-up-view',
  templateUrl: './sign-up-view.component.html',
  styleUrls: ['./sign-up-view.component.css']
})
export class SignUpViewComponent implements OnInit {

  inscriptionForm: FormGroup;
  errorMsg: any;

  constructor(private authService: AuthService,
              private router: Router,
              private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    firebase.auth().onAuthStateChanged(
      (user) => {
        if (user) {
          this.router.navigate(['series']);
        }
      });

    this.inscriptionForm = this.formBuilder.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', Validators.required]
    });
  }

  onSubmitSignUp(): void {
    const email = this.inscriptionForm.value.email;
    const password = this.inscriptionForm.value.password;

    this.authService.createNewUser(email, password)
      .then(() => {
        this.router.navigate(['/series']);
      })
      .catch(error => {
          this.errorMsg = error;
        }
      );
  }
}
