import {Component, DoCheck, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SerieService} from '../../services/serie/serie.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Serie} from '../../models/serie.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-edit-serie-view',
  templateUrl: './edit-serie-view.component.html',
  styleUrls: ['./edit-serie-view.component.css']
})
export class EditSerieViewComponent implements OnInit{

  serie: Serie;
  editSerieForm: FormGroup;

  private id = this.route.snapshot.params.id;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private serieService: SerieService,
              private formBuilder: FormBuilder) {

    this.editSerieForm = this.formBuilder.group({
      name: ['', Validators.required],
      dateBegin: ['', Validators.required],
      nbrSeasons: ['', Validators.required],
      description: ['', Validators.required],
      critic: ['', Validators.required],
      photo: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.serie = new Serie('', null, null, '', '', '');
    this.serieService.getSingleSerie(+this.id).then(
      (serie: Serie) => {
        this.serie = serie;
        this.editSerieForm.controls['name'].setValue(this.serie.name);
        this.editSerieForm.controls['dateBegin'].setValue(new Date(this.serie.dateBegin).toISOString().split('T')[0]);
        this.editSerieForm.controls['nbrSeasons'].setValue(this.serie.nbrSeasons);
        this.editSerieForm.controls['description'].setValue(this.serie.description);
        this.editSerieForm.controls['critic'].setValue(this.serie.critic);
        this.editSerieForm.controls['photo'].setValue(this.serie.photo);
      });
  }

  onSubmitEditSerie(): void {
    const name = this.editSerieForm.value.name;
    const dateBegin = Date.parse(this.editSerieForm.value.dateBegin);
    const nbrSeasons = this.editSerieForm.value.nbrSeasons;
    const description = this.editSerieForm.value.description;
    const critic = this.editSerieForm.value.critic;
    const photo = this.editSerieForm.value.photo;

    const editSerie = new Serie(name, dateBegin, nbrSeasons, description, critic, photo);

    if (this.serie.comment === undefined){
      this.serie.comment = [];
    }
    editSerie.comment = this.serie.comment;

    this.serieService.editSerie(editSerie, +this.id);
    this.router.navigate(['/serie/' + this.id]);
  }
}
