import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {SerieService} from '../../services/serie/serie.service';
import {Serie} from '../../models/serie.model';
import {Comment} from '../../models/commentaire.model';

@Component({
  selector: 'app-new-serie-view',
  templateUrl: './new-serie-view.component.html',
  styleUrls: ['./new-serie-view.component.css']
})
export class NewSerieViewComponent implements OnInit {

  newSerieForm: FormGroup;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private serieService: SerieService) { }

  ngOnInit(): void {
    this.newSerieForm = this.formBuilder.group({
      name: ['', Validators.required],
      dateBegin: ['', Validators.required],
      nbrSeasons: ['', Validators.required],
      description: ['', Validators.required],
      critic: ['', Validators.required],
      photo: ['', Validators.required]
    });
  }

  onSubmitNewSerie(): void {
    const name = this.newSerieForm.value.name;
    let dateBegin = Date.parse(this.newSerieForm.value.dateBegin);
    const nbrSeasons = this.newSerieForm.value.nbrSeasons;
    const description = this.newSerieForm.value.description;
    const critic = this.newSerieForm.value.critic;
    const photo = this.newSerieForm.value.photo;

    if (isNaN(dateBegin)){
      dateBegin = null;
    }

    const newSerie = new Serie(name, dateBegin, nbrSeasons, description, critic, photo);

    this.serieService.createNewSerie(newSerie);
    this.router.navigate(['/series']);
  }
}
