import {Component, OnDestroy, OnInit} from '@angular/core';
import {Serie} from '../../models/serie.model';
import {SerieService} from '../../services/serie/serie.service';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Comment} from '../../models/commentaire.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-single-serie-view',
  templateUrl: './single-serie-view.component.html',
  styleUrls: ['./single-serie-view.component.css']
})
export class SingleSerieViewComponent implements OnInit, OnDestroy{

  serie: Serie;
  serieSubscription: Subscription;

  errorMsg: string;

  newCommentForm: FormGroup;

  editURL: any;

  private id = this.route.snapshot.params.id;

  constructor(private serieService: SerieService,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.serieSubscription = this.serieService.seriesSubject
      .subscribe((series: Array<Serie>) => {
        this.serie = series[+this.id];
      });
    this.serieService.emitSeriesSubject();

    this.newCommentForm = this.formBuilder.group({
      comment: ['', Validators.required],
    });

    this.editURL = '/serie/edit/' + this.id;
  }

  /**
   * Method that calls the service to add a comment
   */
  onSubmitNewComment(): void {
    if (this.newCommentForm.value.comment !== ''){
      this.errorMsg = null;
      this.serieService.addCommentToSerie(new Comment(this.newCommentForm.value.comment), +this.id);
      this.newCommentForm.controls['comment'].setValue('');
    }else{
      this.errorMsg = 'Le commentaire ne peut pas être vide.';
    }
  }

  ngOnDestroy(): void {
    this.serieSubscription.unsubscribe();
  }
}
