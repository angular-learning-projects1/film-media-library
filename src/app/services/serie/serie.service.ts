import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import * as firebase from 'firebase';
import DataSnapshot = firebase.database.DataSnapshot;
import {Serie} from '../../models/serie.model';
import {Comment} from '../../models/commentaire.model';

@Injectable({
  providedIn: 'root'
})
export class SerieService {

  series: Array<Serie>;
  seriesSubject = new Subject<Array<Serie>>();

  constructor() {
    this.series = [];
    this.getSeries();
  }

  emitSeriesSubject(): void {
    this.seriesSubject.next(this.series);

  }

  /**
   * Method for saves the list on a node of the database
   */
  saveSeries(): void {
    firebase.database().ref('/series').set(this.series);
  }

  // ================ CRUD ================ //

  /**
   * Method for create a new serie in Firebase
   * @param newSerie
   */
  // -------- Create -------- //
  createNewSerie(newSerie: Serie): void {
    this.series.push(newSerie);
    this.saveSeries();
    this.emitSeriesSubject();
  }

  /**
   * Method for add a comment in a serie
   * @param newComment
   * @param id
   */
  addCommentToSerie(newComment: Comment, id: number): void {
    if (this.series[id].comment === undefined){
      this.series[id].comment = [];
    }
    this.series[id].comment.push(newComment);
    this.saveSeries();
    this.emitSeriesSubject();
  }

  // -------- Read -------- //
  /**
   * Method for retrieves all the series in Firebase
   */
  getSeries(): void {
    firebase.database().ref('/series')
      .on('value', (data: DataSnapshot) => {
          this.series = data.val() ? data.val() : [];
          this.emitSeriesSubject();
        });
  }

  /**
   * Method for retrieves a serie in Firebase with an id
   * @param id
   */
  getSingleSerie(id: number): Promise<any> {
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/series/' + id).once('value')
          .then((data: DataSnapshot) => {
            resolve(data.val());
          })
          .catch((error) => {
            reject(error);
          });
      });
  }

  /**
   * Method for update a serie in Firebase with an index
   * @param editedSerie
   * @param index
   */
  // -------- Update -------- //
  editSerie(editedSerie: Serie, id: number): void {
    this.series[id] = editedSerie;
    this.saveSeries();
    this.emitSeriesSubject();
  }

  // -------- Delete -------- //
  /**
   * Method for delete a serie in Firebase
   * @param book
   */
  removeSerie(indexSerie: number): void {
    this.series.splice(indexSerie, 1);
    this.saveSeries();
    this.emitSeriesSubject();
  }

}
