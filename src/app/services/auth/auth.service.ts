import {Injectable, OnInit} from '@angular/core';
import * as firebase from 'firebase';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService{

  constructor(private router: Router) { }

  /**
   * Method for create a new user
   * @param email
   * @param password
   */
  createNewUser(email: string, password: string): Promise<any> {
    return new Promise(
      (resolve, reject) => {
        firebase.auth().createUserWithEmailAndPassword(email, password)
          .then(() => {
            resolve();
          })
          .catch(error => {
            reject(error);
          });
      });
  }

  /**
   * Method for connect a user
   * @param email
   * @param password
   */
  signInUser(email: string, password: string): Promise<any> {
    return new Promise(
      (resolve, reject) => {
        firebase.auth().signInWithEmailAndPassword(email, password)
          .then(() => {
            resolve();
          })
          .catch(() => {
            reject('Le couple email/mot de passe est incorrect.');
          });
      });
  }

  /**
   * Method for disconnect a user
   */
  signOutUser(): void {
    firebase.auth().signOut();
    this.router.navigate(['auth']);
  }
}
